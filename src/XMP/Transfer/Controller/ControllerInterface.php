<?php

namespace XMP\Transfer\Controller;


interface ControllerInterface {

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getResponse();
} 