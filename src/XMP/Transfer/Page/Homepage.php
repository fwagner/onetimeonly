<?php
/**
 * Created by PhpStorm.
 * User: arno
 * Date: 07/06/14
 * Time: 11:44
 */

namespace XMP\Transfer\Page;

use Symfony\Component\HttpFoundation\Response;
use XMP\Transfer\Controller\Controller;

class Homepage extends Controller
{
    /**
     * @throws \RuntimeException when openssl seems to be broken
     * @return Response
     */
    public function getResponse()
    {
        $response = new Response();

        // Generate some random bytes for the client side
        $bytes = openssl_random_pseudo_bytes(512/8, $strong);
        if (!$strong) {
            throw new \RuntimeException("Secure random source is broken!");
        }

        $template = $this->twig->loadTemplate('homepage.twig');
        $response->setContent($template->render([
            'entropy' => base64_encode($bytes)
        ]));
        $response->setMaxAge(3600);
        return $response;
    }

}