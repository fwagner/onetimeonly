<?php

namespace XMP\Transfer\Page;

use Symfony\Component\HttpFoundation\Response;
use XMP\Transfer\Controller\Controller;

class Faq extends Controller
{
    /**
     * @return Response
     */
    public function getResponse()
    {
        $response = new Response();
        $template = $this->twig->loadTemplate('faq.twig');
        $response->setContent($template->render([]));
        $response->setMaxAge(86400);
        return $response;
    }

}