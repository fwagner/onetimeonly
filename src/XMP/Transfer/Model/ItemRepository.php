<?php
namespace XMP\Transfer\Model;


use XMP\Transfer\Configuration;

class ItemRepository {

    /**
     * @var \mysqli
     */
    private $db;

    public function __construct()
    {
        $db = null;
    }

    public function store(Item $item)
    {
        if (!$this->connectDB())
            return false;
        $stmt = $this->db->prepare('insert into items (id, created, value) VALUES (?, ?, ?)');
        if (!$stmt)
            return false;
        $timestamp = $item->created->format('Y-m-d H:i:s');
        if (!$stmt->bind_param('sss', $item->id, $timestamp, $item->value))
            return false;
        if (!$stmt->execute())
            return false;
        if ($stmt->errno || $stmt->affected_rows !== 1)
            return false;

        return true;
    }

    public function getById($id)
    {
        if (!$this->connectDB())
            return false;
        $stmt = $this->db->prepare('select id, created, value from items where id=?');
        if (!$stmt)
            return false;
        if (!$stmt->bind_param('s', $id))
            return false;
        $item = new Item();
        $stmt->bind_result($item->id, $item->created, $item->value);
        if (!$stmt->execute())
            return false;
        if ($stmt->errno)
            return false;
        $res = $stmt->fetch();
        if ($res === false)
            return false;
        if ($res === null)
            return null;
        return $item;
    }

    public function deleteById($id)
    {
        if (!$this->connectDB())
            return false;
        $stmt = $this->db->prepare('delete from items where id=?');
        if (!$stmt)
            return false;
        if (!$stmt->bind_param('s', $id))
            return false;
        if (!$stmt->execute())
            return false;
        if ($stmt->errno || $stmt->affected_rows !== 1)
            return false;
        return true;
    }

    private function connectDB()
    {
        if (!is_null($this->db))
            return true;
        $db = new \mysqli(Configuration::DB_HOST, Configuration::DB_USER, Configuration::DB_PWD, Configuration::DB_NAME);
        if (!$db || $db->connect_errno)
            return false;
        $this->db = $db;
        return true;
    }
}