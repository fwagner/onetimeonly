<?php
/**
 * Created by PhpStorm.
 * User: arno
 * Date: 07/06/14
 * Time: 12:09
 */

namespace XMP\Transfer\Model;


class Item {
    /**
     * @var string
     */
    public $id;

    /**
     * @var \DateTime
     */
    public $created;

    /**
     * @var string
     */
    public $value;


    /**
     *
     */
    public function generateId()
    {
        $id = '';
        while (strlen($id) < 25) {
            $bytes = openssl_random_pseudo_bytes(256/8, $strong);
            if (!$strong) {
                throw new \RuntimeException("Secure random source is broken!");
            }
            $bytes = hash('sha256', $bytes, true);
            $id = base64_encode($bytes);
            $id = preg_replace('#[+/]#', '', $id);
            $id = substr($id, 0, 25);
        }
        $this->id = $id;
        return $id;
    }
}