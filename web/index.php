<?php
$loader = require __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use XMP\Transfer\Controller\Controller;
use XMP\Transfer\Page\View;

$request = Request::createFromGlobals();

$map = array(
    '/' => 'XMP\Transfer\Page\Homepage',
    '/save' => 'XMP\Transfer\Page\Save',
    '/faq' => 'XMP\Transfer\Page\Faq',
);

$twigLoader = new Twig_Loader_Filesystem(__DIR__.'/../Resources/templates');
//$twig = new Twig_Environment($twigLoader, ['cache' => __DIR__.'/../Resources/templates-compiled']);
$twig = new Twig_Environment($twigLoader);


$path = $request->getPathInfo();
if (isset($map[$path])) {
    /** @var Controller $controller */
    $controller = new $map[$path]($request, $twig);
    $response = $controller->getResponse();
}
elseif (preg_match('#^/[a-zA-Z0-9]{25}$#', $path)) {
    $controller = new View($request, $twig);
    $response = $controller->getResponse();
}
else {
    $response = new Response();
    $response->setStatusCode(404);
    $response->setContent('Not Found');
}

$response->send();