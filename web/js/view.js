// if this id exists, the preliminary checks were successful
if ($('#msgContent')) {
    if (location.hash.length == 26) {
        loadAndDecryptMessage();
    } else {
        $('#showMessage').replaceWith(
            '<div class="container">' +
            '<div class="alert alert-danger">' +
            '<p><strong>Error:</strong> ' +
            'The URL is incomplete. Please make sure to copy the entire URL. ' +
            'After correcting, do a full page reload (otherwise this message does not go away.)</p>' +
            '</div></div>'
        );
    }
}